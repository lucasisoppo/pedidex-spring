package com.jhonystein.pedidex.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "REGRAS")
@SequenceGenerator(name = "REGRAS_SEQ")
public class Regra implements GrantedAuthority {
    
    @Id
    @Column(name = "ID_REGRA")
    @GeneratedValue(generator = "REGRAS_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Column(name = "REGRA", unique = true)
    private String regra;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegra() {
        return regra;
    }

    public void setRegra(String regra) {
        this.regra = regra;
    }

    @Override
    public String getAuthority() {
        return regra;
    }
}
