package com.jhonystein.pedidex.resources;

import com.jhonystein.pedidex.models.Usuario;
import com.jhonystein.pedidex.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthResource {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @PostMapping("/login")
    public Usuario login(@RequestBody Usuario usuario) {
        return usuarioService.login(usuario);
    }
    
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registro(@RequestBody Usuario usuario) {
        return usuarioService.register(usuario);
    }
    
    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> 
        handleAuthenticationException(AuthenticationException e) {
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(e.getMessage());
        }
    
}
